﻿using System.Collections;
using System.Collections.Generic;
using TowerDefense.Units;
using UnityEngine;

namespace TowerDefense
{
	public class GameController : MonoBehaviour
	{
		private List<Unit> _curUnits;
		private int _wavesCount;
		private int _curWaveIndex = 0;
		private int _curWave = 0;
		private static GameController _this;

		[SerializeField]
		private int _health = 10;
		[SerializeField]
		private int _coins = 100;
		[SerializeField]
		private List<WavePropertiesData> _waves;
		[SerializeField]
		private Vector3 _spawnPoint;
		[SerializeField]
		private float _firstWaveDelay = 5f;
		[SerializeField]
		private List<Transform> _moveTargetsForUnits;

		public int GetHealth => _health;
		public int GetCoins => _coins;
		public int GetWavesCount => _wavesCount;
		public int GetCurWave => _curWave;
		public static GameController This { get; private set; }

		public void TakeCoins(int coins) => _coins -= coins;

		private void Awake()
		{
			This = this;
		}

		private void Start()
		{
			_curUnits = new List<Unit>();
			_wavesCount = _waves.Count;
			StartCoroutine(NextWave());
		}

		public void AddCoins(int coins) => _coins += coins;

		private IEnumerator NextWave()
		{
			if (_curWaveIndex == 0)
			{
				yield return new WaitForSeconds(_firstWaveDelay);
			}

			_curWave++;

			var unitPrefab = _waves[_curWaveIndex].UnitPrefab;
			var unitsCount = _waves[_curWaveIndex].UnitsCount;
			var unitSpawnDelay = _waves[_curWaveIndex].UnitSpawnDelay;

			for (int i = 0; i < unitsCount; i++)
			{
				var unit = Instantiate(unitPrefab, _spawnPoint, new Quaternion());
				var baseUnit = unit.GetComponent<Unit>();
				baseUnit.SetMoveTargets(_moveTargetsForUnits);
				baseUnit.SetGameController(this);
				_curUnits.Add(baseUnit);

				yield return new WaitForSeconds(unitSpawnDelay);
			}

			_curWaveIndex++;
		}

		public void TakeDamage()
		{
			_health--;

			if (_health <= 0)
			{
#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
#else
			//todo
#endif
			}
		}

		public void RemoveUnit(Unit baseUnit)
		{
			_curUnits.Remove(baseUnit);
			CheckNextWave();
		}

		private void CheckNextWave()
		{
			if (_curUnits.Count == 0 && _curWaveIndex != _wavesCount)
			{
				StartCoroutine(NextWave());
			}
			else if (_curUnits.Count == 0 && _curWaveIndex == _wavesCount)
			{
				LevelComplete();
			}
		}

		private void LevelComplete()
		{
			Debug.Log("Win");
		}
	}
}