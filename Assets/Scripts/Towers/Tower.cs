﻿using System.Collections.Generic;
using TowerDefense.Units;
using UnityEngine;

namespace TowerDefense.Towers
{
	public class Tower : MonoBehaviour
	{
		[SerializeField]
		private Transform _tower;
		[SerializeField]
		private GameObject _bulletPrefab;
		[SerializeField]
		private Transform _bulletPoint;
		[SerializeField]
		private List<UpgradeTowerPropertiesData> _upgrades;

		private int _damage = 15;
		private float _startShootDelay = 0.5f;
		private int _cost = 100;
		private float _range = 1.8f;

		private List<GameObject> _unitsInRange;
		private GameObject _target;
		private float _shootDelay;
		private CircleCollider2D _collider;
		private SpriteRenderer _spriteRenderer;
		private int _level = 1;

		public int GetCost => _cost;
		public float GetRange => _range;
		public bool IsMaxLevel => _level >= 3;

		private void Start()
		{
			_unitsInRange = new List<GameObject>();
			_shootDelay = _startShootDelay;
			_collider = GetComponent<CircleCollider2D>();
			_spriteRenderer = _tower.GetComponent<SpriteRenderer>();

			FillCharacteristics();
		}

		private void FillCharacteristics()
		{
			_range = _upgrades[_level - 1].Range;
			_startShootDelay = _upgrades[_level - 1].AttackSpeed;
			_damage = _upgrades[_level - 1].Damage;
			_cost = _upgrades[_level - 1].Cost;

			_collider.radius = _range;
			_spriteRenderer.sprite = _upgrades[_level - 1].Image;
		}

		public void UpgradeLevel()
		{
			_level++;
			FillCharacteristics();
		}

		public int GetCostForNextLevel()
		{
			if (IsMaxLevel)
			{
				return _upgrades[_level - 1].Cost;
			}
			else
			{
				return _upgrades[_level].Cost;
			}
		}

		private void Update()
		{
			FindTarget();
			RotateToTarget();
			ShootToTarget();
		}

		private void FindTarget()
		{
			float minDist = float.MaxValue;
			GameObject target = null;

			foreach (var unit in _unitsInRange)
			{
				float dist = unit.GetComponent<Unit>().GetDistanceToExit();
				if (dist < minDist)
				{
					target = unit;
					minDist = dist;
				}
			}

			_target = target;
		}

		private void RotateToTarget()
		{
			if (_target == null) return;

			Vector3 targetDir = _target.transform.position - _tower.position;
			float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
			Quaternion newRot = Quaternion.AngleAxis(angle, Vector3.forward);
			_tower.rotation = newRot;
		}

		private void ShootToTarget()
		{
			_shootDelay -= Time.deltaTime;
			if (_target == null) return;

			if (_shootDelay <= 0f)
			{
				var bullet = Instantiate(_bulletPrefab, _bulletPoint.position, _tower.rotation);
				var bulletComp = bullet.GetComponent<Bullet>();
				bulletComp.SetDamage(_damage);
				bulletComp.SetTarget(_target.transform);

				_shootDelay = _startShootDelay;
			}
		}

		private void OnUnitDestroy(GameObject unit)
		{
			_unitsInRange.Remove(unit);
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.tag == "Unit")
			{
				_unitsInRange.Add(collision.gameObject);
				var unit = collision.GetComponent<Unit>();
				unit.unitDelegate += OnUnitDestroy;
			}
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			if (collision.tag == "Unit")
			{
				_unitsInRange.Remove(collision.gameObject);
				var unit = collision.GetComponent<Unit>();
				unit.unitDelegate -= OnUnitDestroy;
			}
		}
	}
}