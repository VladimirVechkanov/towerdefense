﻿using System.Collections;
using TowerDefense.Units;
using UnityEngine;

namespace TowerDefense.Towers
{
	public class Bullet : MonoBehaviour
	{
		[SerializeField]
		private float _speed = 5f;
		[SerializeField]
		private float _liveTime = 5f;
		[SerializeField]
		private bool _isBreaking;

		private int _damage = 5;
		private Transform _target;

		private void Start()
		{
			StartCoroutine(Die());
		}

		private void Update()
		{
			if (_target != null && _isBreaking)
			{
				MoveToTarget();
				RotateToTarget();
			}
			else
			{
				MoveForward();
			}
		}

		public void SetDamage(int damage) => _damage = damage;
		public void SetTarget(Transform target) => _target = target;

		private void MoveForward()
		{
			transform.position += transform.right * Time.deltaTime * _speed;
		}

		private void MoveToTarget()
		{
			transform.position = Vector3.MoveTowards(transform.position, _target.position, Time.deltaTime * _speed);
		}

		private void RotateToTarget()
		{
			if (_target == null) return;

			Vector3 targetDir = _target.transform.position - transform.position;
			float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
			Quaternion newRot = Quaternion.AngleAxis(angle, Vector3.forward);
			transform.rotation = newRot;
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.tag == "Unit")
			{
				collision.GetComponent<Unit>().TakeDamage(_damage);
				if (_isBreaking)
				{
					Destroy(gameObject);
				}
			}
		}

		private IEnumerator Die()
		{
			yield return new WaitForSeconds(_liveTime);
			Destroy(gameObject);
		}
	}
}