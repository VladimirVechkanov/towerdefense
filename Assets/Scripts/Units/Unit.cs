﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense.Units
{
	public class Unit : MonoBehaviour
	{
		private Transform _target;
		private GameController _gameController;
		private List<Transform> _moveTargets;
		private bool _isDestroyed;

		[SerializeField]
		private float _speed = 5f;
		[SerializeField]
		private float _rotateSpeed = 5f;
		[SerializeField]
		private float _health = 100;
		[SerializeField]
		private int _reward = 5;

		public int GetReward => _reward;
		public float GetHealth => _health;

		public delegate void UnitDelegate(GameObject unit);
		public UnitDelegate unitDelegate;

		private void Update()
		{
			Move();
		}

		private void OnTriggerStay2D(Collider2D collision)
		{
			if (_target != null && transform.position == _target.position && collision.transform == _target.transform)
			{
				SetTatget();
			}
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.tag == "Exit")
			{
				_gameController.TakeDamage();
				DestroyUnit();
			}
		}

		public void SetMoveTargets(List<Transform> moveTargets)
		{
			_moveTargets = new List<Transform>();
			_moveTargets.AddRange(moveTargets);
			SetTatget();
		}

		public void SetGameController(GameController gameController) => _gameController = gameController;

		private void SetTatget()
		{
			if (_moveTargets.Count == 0) return;

			_target = _moveTargets[0];
			_moveTargets.Remove(_moveTargets[0]);
			StartCoroutine(RotateToTarget());
		}

		private IEnumerator RotateToTarget()
		{
			Vector3 targetDir = _target.transform.position - transform.position;
			float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
			Quaternion endRotation = Quaternion.AngleAxis(angle, Vector3.forward);

			var accelerator = 1f;

			while (transform.rotation != endRotation)
			{
				transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, _rotateSpeed * Time.deltaTime * accelerator);
				accelerator++;

				yield return null;
			}
		}

		private void Move()
		{
			if (_target == null) return;

			transform.position = Vector3.MoveTowards(transform.position, _target.position, _speed * Time.deltaTime);
		}

		public void TakeDamage(int damage)
		{
			_health -= damage;

			if (_health <= 0)
			{
				DestroyUnit(true);
			}
		}

		private void DestroyUnit(bool addReward = false)
		{
			if (_isDestroyed) return;

			_isDestroyed = true;

			if (addReward)
			{
				_gameController.AddCoins(_reward);
			}

			_gameController.RemoveUnit(this);
			Destroy(gameObject);
		}

		public float GetDistanceToExit()
		{
			float distance = 0f;

			if (_moveTargets.Count == 0)
			{
				distance = Vector3.Distance(transform.position, _target.position);
			}
			else if (_moveTargets.Count == 1)
			{
				distance += Vector3.Distance(transform.position, _target.position);
				distance += Vector3.Distance(_target.position, _moveTargets[0].position);
			}
			else
			{
				Vector3 startPos = transform.position;
				Vector3 endPos = _target.position;

				distance += Vector3.Distance(startPos, endPos);

				for (int i = 0; i < _moveTargets.Count - 1; i++)
				{
					startPos = _moveTargets[i].position;
					endPos = _moveTargets[i + 1].position;
					distance += Vector3.Distance(startPos, endPos);
				}
			}

			return distance;
		}

		private void OnDestroy()
		{
			if (unitDelegate != null)
			{
				unitDelegate(gameObject);
			}
		}
	}
}