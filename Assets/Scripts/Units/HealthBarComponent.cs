﻿using UnityEngine;

namespace TowerDefense.Units
{
	public class HealthBarComponent : MonoBehaviour
	{
		[SerializeField]
		private Unit _unit;
		[SerializeField]
		private Transform _healthBarFront;
		[SerializeField]
		private Quaternion _startRot;

		private float _maxHealth;

		private void Start()
		{
			_maxHealth = _unit.GetHealth;
		}

		private void Update()
		{
			transform.rotation = _startRot;

			if (_unit.GetHealth > 0)
			{
				_healthBarFront.localScale = new Vector3(transform.localScale.x, _unit.GetHealth / _maxHealth, transform.localScale.z);
			}
		}
	}
}