﻿using System.Linq;
using TowerDefense.Towers;
using UnityEngine;

namespace TowerDefense.Clicks
{
	public class GrassComponent : BaseClickComponent
	{
		[SerializeField]
		private float _rangeMultiplier = 1.8f;
		[SerializeField]
		private GameObject _canvas;
		[SerializeField]
		private GameObject _panelSelection;
		[SerializeField]
		private GameObject _panelUpgrade;
		[SerializeField]
		private GameObject _towerRange;
		[SerializeField]
		private GameObject _upgradeButton;
		[SerializeField]
		private GameObject _machinegunPrefab;
		[SerializeField]
		private GameObject _rocketLauncherPrefab;
		[SerializeField]
		private GameObject _snipergunPrefab;

		private GameController _gameController;

		public Tower TowerInst { get; private set; }

		private void Start()
		{
			_gameController = GameController.This;
			_canvas.SetActive(true);
		}

		protected override void ShowHideSelection()
		{
			if (CloseOtherPanels()) return;

			if (CanPlaceTower())
			{
				_panelSelection.SetActive(!_panelSelection.activeSelf);
			}
			else
			{
				_panelUpgrade.SetActive(!_panelUpgrade.activeSelf);

				ScaleRangeTower();
				_towerRange.SetActive(true);
			}
		}

		private void ScaleRangeTower()
		{
			_towerRange.transform.localScale = new Vector3(TowerInst.GetRange * _rangeMultiplier, TowerInst.GetRange * _rangeMultiplier, TowerInst.GetRange * _rangeMultiplier);
		}

		private bool CanPlaceTower()
		{
			return TowerInst == null;
		}

		private bool CloseOtherPanels()
		{
			var panels = GameObject.FindGameObjectsWithTag("Panel").Where(x => x != _panelSelection && x != _panelUpgrade && x != _towerRange && x.activeSelf == true).ToList();
			foreach (var panel in panels)
			{
				panel.SetActive(false);
			}

			return panels.Count != 0;
		}

		public void PlaceMachinegun_UnityEditor()
		{
			var cost = _machinegunPrefab.GetComponent<Tower>().GetCost;
			if (_gameController.GetCoins < cost) return;

			TowerInst = Instantiate(_machinegunPrefab, transform.position, Quaternion.Euler(0f, 0f, 0f)).GetComponent<Tower>();
			_gameController.TakeCoins(cost);
			_panelSelection.SetActive(false);
			ScaleRangeTower();
		}

		public void PlaceRocketLauncher_UnityEditor()
		{
			var cost = _rocketLauncherPrefab.GetComponent<Tower>().GetCost;
			if (_gameController.GetCoins < cost) return;

			TowerInst = Instantiate(_rocketLauncherPrefab, transform.position, Quaternion.Euler(0f, 0f, 0f)).GetComponent<Tower>();
			_gameController.TakeCoins(cost);
			_panelSelection.SetActive(false);
			ScaleRangeTower();
		}

		public void PlaceSnipergun_UnityEditor()
		{
			var cost = _snipergunPrefab.GetComponent<Tower>().GetCost;
			if (_gameController.GetCoins < cost) return;

			TowerInst = Instantiate(_snipergunPrefab, transform.position, Quaternion.Euler(0f, 0f, 0f)).GetComponent<Tower>();
			_gameController.TakeCoins(cost);
			_panelSelection.SetActive(false);
			ScaleRangeTower();
		}

		public void UpgradeTower_UnityEditor()
		{
			if (!_upgradeButton.activeSelf) return;

			var costForNextLevel = TowerInst.GetCostForNextLevel();
			if (_gameController.GetCoins < costForNextLevel) return;

			_gameController.TakeCoins(costForNextLevel);
			TowerInst.UpgradeLevel();
			ScaleRangeTower();

			if (TowerInst.IsMaxLevel)
			{
				_upgradeButton.SetActive(false);
				_panelUpgrade.SetActive(false);
				return;
			}

			_panelUpgrade.SetActive(false);
		}

		public void DeleteTower_UnityEditor()
		{
			_gameController.AddCoins(TowerInst.GetCost * 75 / 100);
			Destroy(TowerInst.gameObject);
			TowerInst = null;

			_upgradeButton.SetActive(true);
			_panelUpgrade.SetActive(false);
		}
	}
}