﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace TowerDefense.Clicks
{
	public abstract class BaseClickComponent : MonoBehaviour, IPointerClickHandler
	{
		private Camera _camera;

		private void Start()
		{
			_camera = Camera.main;
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			ShowHideSelection();
		}

		protected virtual void ShowHideSelection()
		{
			throw new System.NotImplementedException();
		}
	}
}