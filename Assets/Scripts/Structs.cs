﻿using System;
using UnityEngine;

namespace TowerDefense
{
	[Serializable]
	public struct WavePropertiesData
	{
		public GameObject UnitPrefab;
		public int UnitsCount;
		public float UnitSpawnDelay;
	}

	[Serializable]
	public struct UpgradeTowerPropertiesData
	{
		public Sprite Image;
		public float Range;
		public float AttackSpeed;
		public int Damage;
		public int Cost;
	}
}