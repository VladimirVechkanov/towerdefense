﻿using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.UI
{
    public class UIComponent : MonoBehaviour
    {
        [SerializeField]
        private Text _healthText;
        [SerializeField]
        private Text _coinsText;
        [SerializeField]
        private Text _wavesText;

        private GameController _gameController;

        private void Start()
        {
            _gameController = FindObjectOfType<GameController>();
        }

        private void Update()
        {
            SetInformation();
        }

        private void SetInformation()
        {
            _healthText.text = _gameController.GetHealth.ToString();
            _coinsText.text = _gameController.GetCoins.ToString();
            _wavesText.text = $"{_gameController.GetCurWave}/{_gameController.GetWavesCount}";
        }
    }
}